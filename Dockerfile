FROM python:3.6
ADD . /rabbit
WORKDIR /rabbit
RUN pip install -r requirements.txt